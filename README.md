# OpenML dataset: Water_Quality

https://www.openml.org/d/46085

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset named "water-quality-1.csv" comprises a comprehensive collection of water quality measurements from various sites, meticulously recorded to monitor environmental health and pollution levels. It encompasses a diverse range of parameters such as Fecal Coliform, Conductivity Field, Temperature, Total Nitrogen, and Nitrite + Nitrate Nitrogen, crucial for assessing water quality in ecosystems. The data spans multiple years, offering insights into temporal changes affecting water bodies.

Attribute Description:
- Sample ID: Unique identifier for each sample (e.g., 58086).
- Grab ID: Identifier for the specific collection instance, with some entries missing.
- Profile ID: Unique profile number associated with each sample site (e.g., 46937).
- Sample Number: A distinct code for each sample, combining letters and numbers (e.g., 'L47270-122').
- Collect DateTime: Date and time when the sample was collected, in MM/DD/YYYY HH:MM:SS AM/PM format.
- Depth (m): Depth at which the sample was collected, in meters (e.g., 1.0).
- Site Type: Classification of the water body from which the sample was taken (e.g., Large Lakes).
- Area: Geographic location or name of the water body (e.g., Central Puget Sound).
- Locator: A unique code for the site's location (e.g., KTHA03).
- Site: Detailed description of the sample location (e.g., Lake Sammamish near Issaquah Creek).
- Parameter: The water quality parameter measured (e.g., Fecal Coliform).
- Value: The measured value for the parameter, with some missing entries.
- Units: Measurement units for the parameter values (e.g., umhos/cm).
- QualityId: A numerical value indicating the quality of the data (e.g., 2).
- Lab Qualifier, MDL, RDL, Text Value, Sample Info, Steward Note, Replicates, Replicate Of, Method, Date Analyzed, Data Source: These fields contain additional information about the laboratory procedures, data quality, analysis methods, and sources.

Use Case:
This dataset is invaluable for researchers and environmentalists looking to study water quality trends, identify pollution hotspots, and evaluate the effectiveness of environmental policies over time. It can aid in comparative analysis across different water bodies and help in the formulation of strategies for water conservation and pollution control. Moreover, policymakers can utilize this data to enforce environmental regulations and initiate cleanup efforts in degraded aquatic ecosystems.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46085) of an [OpenML dataset](https://www.openml.org/d/46085). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46085/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46085/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46085/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

